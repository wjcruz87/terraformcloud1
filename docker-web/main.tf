terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
}

provider "docker" {}

resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "web-en-docker"
  ports {
    internal = 80
    external = 8000
  }
  volumes {
    container_path = "/usr/share/nginx/html"
    host_path = "/home/wcruz/gitme/terraformcloud1/docker-web/www"
    read_only = true
  }
}
