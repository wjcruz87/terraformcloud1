terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  profile    = "default"
  region     = "us-east-1"
  shared_credentials_file = "~/.aws/credentials"
}

resource "aws_instance" "terra-aws-instance" {
  	ami 		= "ami-042e8287309f5df03"
  	instance_type	= "t2.micro"
	key_name	= "ubuntu-pair"
}
